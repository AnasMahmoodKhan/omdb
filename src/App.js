import React from "react";
import Footer from "./components/Footer";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Nav from "./components/Nav";
import Home from "./components/Home";
import { Provider } from "react-redux";
import store from "./store/store";

const App = () => {
  return (
    <Provider store={store}>
      <Nav />
      <Router>
        <Switch>
          <Route path="/" component={Home} />
        </Switch>
      </Router>
      <div className="fixed-bottom">
        <Footer />
      </div>
    </Provider>
  );
};

export default App;
