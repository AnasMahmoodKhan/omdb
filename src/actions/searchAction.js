import { APIKEY } from "../ApiKey";
import { SEARCH_MOVIE, FETCH_MOVIES } from "./types";
import axios from "axios";
export const searchMovie = (text) => (dispatch) => {
  dispatch({
    type: SEARCH_MOVIE,
    payload: text,
  });
};

export const fetchMovies = (text) => (dispatch) => {
  axios
    .get(`http://www.omdbapi.com/?i=tt3896198&apikey=${APIKEY}&s=${text}`)
    .then((res) =>
      dispatch({
        type: FETCH_MOVIES,
        payload: res.data.Search,
      })
    )
    .catch((err) => console.log(err));
};
