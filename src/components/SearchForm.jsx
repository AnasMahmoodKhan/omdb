import React, { Component } from "react";
import { connect } from "react-redux";
import { searchMovie,fetchMovies } from "../actions/searchAction";

class SearchForm extends Component {
  handleChange = (e) => {
    this.props.searchMovie(e.target.value);
  };

  handleSubmit=(e)=>{
      e.preventDefault();
      this.props.fetchMovies(this.props.text)
  }
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div class="row ">
          <div class="col-auto">
            <input
              type="text"
              class="form-control"
              id="text"
              placeholder="Search Movies,TV Series..."
              onChange={(e) => this.handleChange(e)}
            />
          </div>
          <br />
          <div class="col-auto text-center p-1">
            <button type="submit" class="btn btn-primary">
              Search
            </button>
          </div>
        </div>
      </form>
    );
  }
}

const mapStateToProps = state=>({
    text : state.movies.text
})

export default connect(mapStateToProps, {searchMovie,fetchMovies})(SearchForm);
