import React, { Component } from 'react';
import { connect } from "react-redux";
import MoviesContainer from './MoviesContainer';
import SearchForm from "./SearchForm";
import Spinner from './Spinner';


class Home extends Component {
render(){
  const {loading} = this.props
  return (
    <React.Fragment>
      <div className="jumbotron">
        <div className="container">
          <h1>IMDb Scraper - Top 250 Movies</h1>
          <p>
            This app is created using React,Redux to scrape the movie details
            from IMDB Top 250 Chart. Videos are displayed on homepage as per the
            count set by admin using paginator.
          </p>
          <SearchForm/>
        </div>
      </div>
      {loading ? <Spinner/> : <MoviesContainer/>}
    </React.Fragment>
  );
}
};

const mapStateToProps = state =>({
  loading : state.movies.loading
})

export default  connect(mapStateToProps)(Home);
