import React from "react";

const Footer = () => {
  return (
    <footer class="bg-light  text-center" style={{backgroundColor: "rgba(0, 0, 0, 0.2)"}}>

    <div class="container p-4 pb-0">
      <form action="">

        <div class="row">

          <div class="col-auto mb-4 mb-md-0">
            <p class="pt-2"><strong>Sign up for Premium Content</strong></p>
          </div>
  
          <div class="col-md-5 col-12 mb-4 mb-md-0">
            <div class="form-outline mb-4">
              <input type="email" id="form5Example2" class="form-control" />
              <label class="form-label" for="form5Example2">Email address</label>
            </div>
          </div>
  
          <div class="col-auto mb-4 mb-md-0">

            <button type="submit" class="btn btn-primary mb-4">Subscribe</button>
          </div>
        </div>

      </form>
    </div>

  

    <div class="text-center p-3" style={{backgroundColor: "rgba(0, 0, 0, 0.2)"}}>
      © 2020 Copyright:

    </div>
    
  </footer>);
};

export default Footer;
