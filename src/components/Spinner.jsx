import React from 'react'

const Spinner = () => {
    return (
        <div className="text-center p-0"  >
            <img src="https://media.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.gif" alt="spinner"/>
        </div>
    )
}

export default Spinner
