import React, { Component } from "react";
import { connect } from "react-redux";


class MoviesContainer extends Component {
  render() {
    const { movies } = this.props;
    let content = movies.length > 0 ? movies : null;
    return(
        <div className="row">
{content
      ? content.map((movie, i) => (
          <div className="col-4" key={i}>
          
              <img
                src="https://www.joblo.com/assets/images/joblo/posters/2019/02/Dyow9RgX4AElAGN.jpg"
                className="card-img-top"
                alt="..."
              />
              <div className="card-body">
                <h5 className="card-title">Toy Story 4</h5>
                <span className="movie_info">2019</span>
                <span className="movie_info float-right">
                  <i className="fas fa-star"></i> 9 / 10
                </span>
              </div>
            </div>
          
        ))
      : null}
		
		</div>
    );
  }
}

const mapStateToProps = (state) => ({
  movies: state.movies.movies,
});

export default connect(mapStateToProps)(MoviesContainer);
